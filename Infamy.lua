-- INFAMY AND LEVELS
if managers.experience and managers.money then
	managers.experience:set_current_rank(25)
	managers.money:_add_to_total(10000000)
	managers.money:_add_to_total(10000000, {no_offshore = true})
	managers.experience:_set_current_level(100)
end
