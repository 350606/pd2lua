-- UNLOCK ALL SKILLS
local function unlock_all_skills()
	managers.skilltree:_set_points(725)

	for tree_id,tree_data in pairs(Global.skilltree_manager.trees) do
		if not tree_data.unlocked then
			managers.skilltree:unlock_tree(tree_id)
		end
		for _,skills in pairs(tweak_data.skilltree.trees[tree_id].tiers) do
			for _,skill_id in ipairs(skills) do
				managers.skilltree:unlock(tree_id, skill_id)
			end
		end
	end
end

unlock_all_skills()
