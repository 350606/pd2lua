-- RESTART
function isHost()
    if not Network then return false end
    return not Network:is_client()
end

function isSinglePlayer()
    return Global.game_settings.single_player or false
end

if isHost() then
    local all_synced = true
    for k, v in pairs(managers.network:session():peers()) do
        if not v:synched() then
            all_synced = false
        end
    end

    if all_synced then
        managers.game_play_central:restart_the_game()
    end

end
