for i=1,12 do
	local binding = GetBinding("VK_F" .. i)
	if binding then
		managers.chat:feed_system_message(ChatManager.GAME, "F" .. i .. ": " .. binding:gsub("scripts/", ""))
	end
end
